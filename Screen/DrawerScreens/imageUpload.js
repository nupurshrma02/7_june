 import React, { Component } from 'react';
 import { View, Text, StyleSheet, Image, Platform,
   Alert, } from 'react-native';
 import { TouchableOpacity } from 'react-native-gesture-handler';
 import Icon from 'react-native-vector-icons/FontAwesome5';
 import ImagePicker from 'react-native-image-picker';
 import RNFetchBlob from 'rn-fetch-blob';
 import AsyncStorage from '@react-native-community/async-storage';
 import axios from 'react-native-axios';
 let data = new FormData();

 export default class imageUpload extends Component {
   constructor(props) {
     super(props)
     this.state = {
         formDataArray: [],
         userid: '',
         image1 : null,
         image2 : null,
         image3 : null,
         imageData1: null,
         imageData2: null,
         imageData3: null,
         imageFilename1: '',
         imageFilename2: '',
         imageFilename3: '',
     }
    }

   componentDidMount() {
     this.checkingAsync();
   }

   checkingAsync = async () => {
     try {
       let user = await AsyncStorage.getItem('userdetails');
       user = JSON.parse(user);
       this.setState({userid : user.id})
       console.log('user id=>', user.id)
       console.log('userData =>', user);
     } catch (error) {
       console.log(error);
     }
   };

   selectingImage1 = () => {
     ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
       console.log('Response = ', responseGet);

       if (responseGet.didCancel) {
         console.log('User cancelled image picker');
       } else if (responseGet.error) {
         console.log('ImagePicker Error: ', responseGet.error);
       } else {
         const source = {uri: responseGet.uri};

         // You can also display the image using data:
         // const source = { uri: 'data:image/jpeg;base64,' + response.data };

         this.setState({
           image1: source,
           imageData1: responseGet.data,
           imageFilename1: responseGet.fileName,
         });
       }
     });
   };

   selectingImage2 = () => {
     ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
       console.log('Response = ', responseGet);

       if (responseGet.didCancel) {
         console.log('User cancelled image picker');
       } else if (responseGet.error) {
         console.log('ImagePicker Error: ', responseGet.error);
       } else {
         const source = {uri: responseGet.uri};

         // You can also display the image using data:
         // const source = { uri: 'data:image/jpeg;base64,' + response.data };

         this.setState({
           image2: source,
           imageData2: responseGet.data,
           imageFilename2: responseGet.fileName,
         });
       }
     });
   };

   selectingImage3 = () => {
     ImagePicker.showImagePicker({quality: 0.3}, responseGet => {
       console.log('Response = ', responseGet);

       if (responseGet.didCancel) {
         console.log('User cancelled image picker');
       } else if (responseGet.error) {
         console.log('ImagePicker Error: ', responseGet.error);
       } else {
         const source = {uri: responseGet.uri};

         // You can also display the image using data:
         // const source = { uri: 'data:image/jpeg;base64,' + response.data };

         this.setState({
           image3: source,
           imageData3: responseGet.data,
           imageFilename3: responseGet.fileName,
         });
       }
     });
   };

//   upload = () => {
//     fetch('http://3.12.158.241/medical/api/image/'+this.state.userid,{
//       method: "POST",
//       headers: {
//           'Accept' : "application/json",
//           'Content-Type': 'application/multipart/form-data'
//       },      
//       body: JSON.stringify(data.append({'image[]':[
//         {
//           name: 'image1',
//           filename: this.state.imageFilename1,
//           type: 'image/jpg',
//           data: this.state.imageData1,
//         },
//         {
//           name: 'image2',
//           filename: this.state.imageFilename2,
//           type: 'image/jpeg',
//           data: this.state.imageData2,
//         },
//         {
//           name: 'image3',
//           filename: this.state.imageFilename3,
//           type: 'image/jpeg',
//           data: this.state.imageData3,
//         },
//       ]}))

//      }).then((res) => res.json())
//     .then(resData => {
//       console.log(resData);
//       console.log('success');
//       alert("Success")
//     }).catch(err => {
//     console.error("error uploading images:", err);
//     //console.log('datttttaa--->', JSON.stringify(data));
//     })
        
//   };

//   upload1 = () => {
//     RNFetchBlob.fetch(
//       'POST',
//       'http://3.12.158.241/medical/api/image/'+this.state.userid,
//       {
//         //Authorization: 'Bearer access-token',
//         //otherHeader: 'foo',
//         'Content-Type': 'multipart/form-data, application/json',
//       },
//       [
//         {
//           name: 'image1',
//           filename: this.state.imageFilename1,
//           type: 'image/jpg',
//           data: this.state.imageData1,
//         },
//         {
//           name: 'image2',
//           filename: this.state.imageFilename2,
//           type: 'image/jpeg',
//           data: this.state.imageData2,
//         },
//         {
//           name: 'image3',
//           filename: this.state.imageFilename3,
//           type: 'image/jpeg',
//           data: this.state.imageData3,
//         },
//       ],
//     )
//       .then(resp => {
//         //this.setState({isLoading: false});
//         console.log('After Posting =>');
//         console.log(resp);
//         //this.setModalVisible(!this.state.modalVisible);
//         //this.props.navigation.navigate('ListingView');
//         alert('success')
//       })
//       .catch(err => {
//         console.log('Error =>' + err);
//         //this.setState({isLoading: false});
//       });
//   }

//   upload2 = () => {
//     const config = {
//       method: 'post',
//       headers: {
//         //'NEEDS_AUTH': true,
//         Accept: 'application/json',
//         'Content-type': 'multipart/form-data'

//       },
//       formDataArray : JSON.stringify([
//         {
//         name: 'image1',
//         filename: this.state.imageFilename1,
//         type: 'image/jpg',
//         data: this.state.imageData1,
//       },
//       {
//         name: 'image2',
//         filename: this.state.imageFilename2,
//         type: 'image/jpeg',
//         data: this.state.imageData2,
//       },
//       {
//         name: 'image3',
//         filename: this.state.imageFilename3,
//         type: 'image/jpeg',
//         data: this.state.imageData3,
//       },
//       ]
//     ),
//       url: 'http://3.12.158.241/medical/api/image/'+this.state.userid,
//       // data: {
//       //   title: postTitle,
//       //   content: postContent,
//       //   location: locationId,
//       //   category: categoryId,

//       // }
//     }

//     axios(config).then(res => console.log('create post ', res)).catch(err => console.log('create post err', err.response)) 
//   }


uploadImageToServer = () => {
 
  RNFetchBlob.fetch('POST', 'http://3.12.158.241/medical/api/image/'+this.state.userid, {
    //Authorization: "Bearer access-token",
    otherHeader: "foo",
    'Content-Type': 'multipart/form-data',
  },JSON.stringify(data.append('image[]', [
      // { name: 'image', filename: this.state.imageFilename1, type: 'image/jpg', data: this.state.imageData1 },
      // { name: 'image', filename: this.state.imageFilename2, type: 'image/jpg', data: this.state.imageData2 },
      // { name: 'image', filename: this.state.imageFilename3, type: 'image/jpg3', data: this.state.imageData3 },
      {name: this.state.image1, imageFilename1: this.state.imageFilename1, type: 'image/jpg', imageData1: this.state.imageData1},
      {name: this.state.image2, imageFilename2: this.state.imageFilename2, type: 'image/jpg', imageData2: this.state.imageData2},
      {name: this.state.image3, imageFilename3: this.state.imageFilename3, type: 'image/jpg', imageData3: this.state.imageData3},
    ]))).then((resp) => {

      var tempMSG = resp.data;

      tempMSG = tempMSG.replace(/^"|"$/g, '');

      Alert.alert(tempMSG);

    }).catch((err) => {
      // ...
      Alert.alert('failed');
    })

}
  

   render() {
     return (
       <View>
       <View style={{flexDirection:'row', justifyContent:'center'}}>
         <TouchableOpacity onPress={this.selectingImage1}>
          <View style={styles.imageContainer}>
          <Image
                       source={
                         this.state.image1 === null
                           ? require('../../Image/plus.png')
                           : this.state.image1
                       }
                       style={{height: 50, width:50}}
                     />
          </View>
         </TouchableOpacity>
         <TouchableOpacity onPress={this.selectingImage2}>
          <View style={styles.imageContainer}>
          <Image
                       source={
                         this.state.image2 === null
                           ? require('../../Image/plus.png')
                           : this.state.image2
                       }
                       style={{height: 50, width:50}}
                     />
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.selectingImage3}>
          <View style={styles.imageContainer}>
          <Image
                       source={
                         this.state.image3 === null
                           ? require('../../Image/plus.png')
                           : this.state.image3
                       }
                       style={{height: 50, width:50}}
                     />
          </View>
          </TouchableOpacity>
         </View>
         <View>
            <TouchableOpacity onPress={this.uploadImageToServer}> 
              <Text>Upload</Text>
            </TouchableOpacity>
         </View>
         </View>
      
     );
   }
 }

 const styles = StyleSheet.create({
     imageContainer:{
       position: 'relative',
       marginTop: 10,
       marginLeft: 10,
       padding: 40,
       backgroundColor: '#fff',
       //height: 120,
       //width: 120,
     },
 });

// import React, { useState } from 'react';
// import {
//   View,
//   SafeAreaView,
//   Text,
//   TouchableOpacity,
//   StyleSheet,
//   Platform,
//   Alert,
//   Image
// } from 'react-native';
// import ImagePicker from 'react-native-image-picker';
// import storage from '@react-native-firebase/storage';
// import * as Progress from 'react-native-progress';

// export default function UploadScreen() {
//   const [image, setImage] = useState(null);
//   const [uploading, setUploading] = useState(false);
//   const [transferred, setTransferred] = useState(0);
//   //...

//   const selectImage = () => {
//     const options = {
//       maxWidth: 2000,
//       maxHeight: 2000,
//       storageOptions: {
//         skipBackup: true,
//         path: 'images'
//       }
//     };
//     ImagePicker.showImagePicker(options, response => {
//       if (response.didCancel) {
//         console.log('User cancelled image picker');
//       } else if (response.error) {
//         console.log('ImagePicker Error: ', response.error);
//       } else if (response.customButton) {
//         console.log('User tapped custom button: ', response.customButton);
//       } else {
//         const source = { uri: response.uri };
//         console.log(source);
//         setImage(source);
//       }
//     });
//   };

//   const uploadImage = async () => {
//     const { uri } = image;
//     const filename = uri.substring(uri.lastIndexOf('/') + 1);
//     const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
//     setUploading(true);
//     setTransferred(0);
//     const task = storage()
//       .ref(filename)
//       .putFile(uploadUri);
//     // set progress state
//     task.on('state_changed', snapshot => {
//       setTransferred(
//         Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
//       );
//     });
//     try {
//       await task;
//     } catch (e) {
//       console.error(e);
//     }
//     setUploading(false);
//     Alert.alert(
//       'Photo uploaded!',
//       'Your photo has been uploaded to Firebase Cloud Storage!'
//     );
//     setImage(null);
//   };

//   return (
      
//     <SafeAreaView style={styles.container}>
//       <TouchableOpacity style={styles.selectButton} onPress={selectImage}>
//         <Text style={styles.buttonText}>Pick an image</Text>
//       </TouchableOpacity>
//       <View style={styles.imageContainer}>
//         {image !== null ? (
//           <Image source={{ uri: image.uri }} style={styles.imageBox} />
//         ) : null}
//         {uploading ? (
//           <View style={styles.progressBarContainer}>
//             <Progress.Bar progress={transferred} width={300} />
//           </View>
//         ) : (
//           <TouchableOpacity style={styles.uploadButton} onPress={uploadImage}>
//             <Text style={styles.buttonText}>Upload image</Text>
//           </TouchableOpacity>
//         )}
//       </View>
//     </SafeAreaView>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     backgroundColor: '#bbded6'
//   },
//   selectButton: {
//     borderRadius: 5,
//     width: 150,
//     height: 50,
//     backgroundColor: '#8ac6d1',
//     alignItems: 'center',
//     justifyContent: 'center'
//   },
//   uploadButton: {
//     borderRadius: 5,
//     width: 150,
//     height: 50,
//     backgroundColor: '#ffb6b9',
//     alignItems: 'center',
//     justifyContent: 'center',
//     marginTop: 20
//   },
//   buttonText: {
//     color: 'white',
//     fontSize: 18,
//     fontWeight: 'bold'
//   },
//   imageContainer: {
//     marginTop: 30,
//     marginBottom: 50,
//     alignItems: 'center',
//     //flexDirection: 'row'
//   },
//   progressBarContainer: {
//     marginTop: 20
//   },
//   imageBox: {
//     width: 300,
//     height: 300
//   }
// });

